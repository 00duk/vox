<?php
namespace vox\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Google_Client;
use Google_Service_Analytics;
use Google_Auth_AssertionCredentials;

class TestCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('ga:test')
            ->setDescription('test');
    }


    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $client = new Google_Client();
        $client->setApplicationName('vox');

        $fileLocator = $this->getContainer()->get('file_locator');
        $key_path = $fileLocator->locate('@voxAdminBundle/Resources/ga/37fc1ced5398.p12');
        $key = file_get_contents($key_path);

        $token_path = $fileLocator->locate('@voxAdminBundle/Resources/ga/token.json');
        $accessToken = file_get_contents($token_path);

        $cred = new Google_Auth_AssertionCredentials(
            '886960317184-c348ccsvpa8n4aprkbge0pp5k65ic068@developer.gserviceaccount.com',
            array('https://www.googleapis.com/auth/analytics.readonly'),
            $key
        );

        //$client->setAccessToken($accessToken);
        $client->setAssertionCredentials($cred);

        /*$client->setClientId('886960317184-c348ccsvpa8n4aprkbge0pp5k65ic068.apps.googleusercontent.com');
        $client->setAccessType('offline_access');*/

        var_dump($client->getAuth()->refreshTokenWithAssertion($cred));

        die;

        if($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }



        $analytics = new Google_Service_Analytics($client);
        $analytics_id   = 'ga:94491599';
        $lastWeek       = date('Y-m-d', strtotime('-52 week'));
        $today          = date('Y-m-d');
        try {
            $optParams = array();
            // Uncomment any of the optional parameters to include them in your query
            //$optParams['dimensions'] = "";
            //$optParams['sort'] = "";
            //$optParams['filters'] = "";
            //$optParams['max-results'] = "";
            $metrics = 'ga:visits';
            $results = $analytics->data_ga->get($analytics_id,
                $lastWeek,
                $today,$metrics,$optParams);
            print_r($results);
        } catch(Exception $e) {
            echo 'There was an error : - ' . $e->getMessage();
        }
    }

}