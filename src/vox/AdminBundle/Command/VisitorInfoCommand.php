<?php
namespace vox\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use vox\AdminBundle\Entity\Visitor;
use vox\AdminBundle\Entity\VisitorHistory;

class VisitorInfoCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('visitor:info:update')
            ->setDescription('get visitor info from api');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();

        $visitors = $em->createQueryBuilder('v')
            ->select('v')
            ->from('voxAdminBundle:Visitor', 'v')
            ->where('v.country IS NULL')
            ->andWhere('v.city IS NULL')
            ->getQuery()
            ->getResult();

        $c=0;
        foreach($visitors as $visitor) {
            if($visitor->getCountry() == null && $visitor->getCity() == null) {

                try{
                    $info = $this->getInfo($visitor->getIp());
                }catch(Exception $e){
                    $logger = $this->getContainer()->get('monolog.logger.vox');
                    $logger->info('[CRON] VisitorInfo: ' . $e->getMessage());
                    break;
                }
                $visitor->setCountry($info['country']);
                $visitor->setCity($info['city']);

                $em->persist($visitor);
                $em->flush();
                $c++;
            }
        }

        if($c > 0){
            echo "Visitors info updated. \n";
            $logger = $this->getContainer()->get('monolog.logger.vox');
            $logger->info('[CRON] '. $c . ' visitor info updated.');
        }
    }

    protected function getInfo($ip) {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://freegeoip.net/json/' . $ip);
        $result=curl_exec($ch);
        curl_close($ch);

        var_dump(json_decode($result));

        $obj = json_decode($result);

        $info['country'] = $obj->country_name == "" ? "Unknown" : $obj->country_name;
        $info['city'] = $obj->city == "" ? "Unknown" : $obj->city;

        return $info;
    }
}