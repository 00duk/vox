<?php
namespace vox\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use vox\AdminBundle\Entity\Visitor;
use vox\AdminBundle\Entity\VisitorHistory;

class VisitorHistoryCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('visitor:history:update')
            ->setDescription('count visitor per day');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $visitorsToday = count($em->getRepository('voxAdminBundle:Visitor')->getVisitorsToday());

        $repository = $em->getRepository('voxAdminBundle:VisitorHistory');

        $today = new \DateTime('now');
        $visitorHistory = $repository->findOneByDate($today);

        if($visitorHistory) {
            $visitorHistory->setVisitorCount($visitorsToday);
        }
        else {
            $visitorHistory = new VisitorHistory();
            $visitorHistory->setDate($today);
            $visitorHistory->setVisitorCount($visitorsToday);
        }

        $em->persist($visitorHistory);
        $em->flush();

        echo "Visitors history updated. \n";

        $logger = $this->getContainer()->get('monolog.logger.vox');
        $logger->info('[CRON] Visitor history updated: ' . $visitorsToday);
    }
}