<?php
namespace vox\AdminBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use vox\AdminBundle\Entity\YoutubeStats;
use vox\AdminBundle\Entity\SoundCloudStats;
use vox\AdminBundle\Entity\FacebookStats;

class MediaUpdateCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('media:update')->setDescription('Update satats for social media');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo $this->youtube();
        echo $this->soundcloud();
        echo $this->facebook();

        $logger = $this->getContainer()->get('monolog.logger.vox');
        $logger->info('[CRON] Social media stats updated');
    }




    public function facebook() {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://graph.facebook.com/voxpopulirecords?access_token='.$this->getContainer()->getParameter('fb_api_id').'|'.$this->getContainer()->getParameter('fb_api_secret'));
        $result=curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result);
        $likes = $json->likes;
        $shares = $json->talking_about_count;

        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('voxAdminBundle:FacebookStats');

        $today = new \DateTime('now');
        $fbStats = $repository->findOneByDate($today);

        if($fbStats) {
            $fbStats->setLikes($likes);
            $fbStats->setShares($shares);
        }
        else {
            $fbStats = new FacebookStats();
            $fbStats->setLikes($likes);
            $fbStats->setShares($shares);
            $fbStats->setDate($today);
        }

        $em->persist($fbStats);
        $em->flush();


        return "facebook stats updated\n";
    }

    public function youtube() {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'https://www.googleapis.com/youtube/v3/channels?part=statistics&forUsername=v0xpopulirecords&key='.$this->getContainer()->getParameter('yt_api_key'));
        $result=curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result);
        $ytResult = $json->items[0]->statistics;

        $viewCount = $ytResult->viewCount;
        $commentCount = $ytResult->commentCount;
        $subscriberCount = $ytResult->subscriberCount;
        $videoCount = $ytResult->videoCount;


        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('voxAdminBundle:YoutubeStats');

        $today = new \DateTime('now');
        $ytStats = $repository->findOneByDate($today);

        if($ytStats) {
            $ytStats->setViewCount($viewCount);
            $ytStats->setCommentCount($commentCount);
            $ytStats->setSubscriberCount($subscriberCount);
            $ytStats->setVideoCount($videoCount);
        }
        else {
            $ytStats = new YoutubeStats();
            $ytStats->setDate($today);
            $ytStats->setViewCount($viewCount);
            $ytStats->setCommentCount($commentCount);
            $ytStats->setSubscriberCount($subscriberCount);
            $ytStats->setVideoCount($videoCount);
        }

        $em->persist($ytStats);
        $em->flush();


        return "youtube updated\n";

    }


    public function soundcloud () {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, 'http://api.soundcloud.com/users/voxpopuli-records?consumer_key='.$this->getContainer()->getParameter('sc_api_key'));
        $result=curl_exec($ch);
        curl_close($ch);

        $json = json_decode($result);
        $followers = $json->followers_count;
        $tracks = $json->track_count;

        $em = $this->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository('voxAdminBundle:SoundCloudStats');

        $today = new \DateTime('now');
        $scStats = $repository->findOneByDate($today);

        if($scStats) {
            $scStats->setFollowers($followers);
            $scStats->setTracks($tracks);
        }
        else {
            $scStats = new SoundCloudStats();
            $scStats->setFollowers($followers);
            $scStats->setTracks($tracks);
            $scStats->setDate($today);
        }

        $em->persist($scStats);
        $em->flush();


        return "soundcloud updated\n";
    }
}