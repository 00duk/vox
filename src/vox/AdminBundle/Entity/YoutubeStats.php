<?php

namespace vox\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * YoutubeStats
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class YoutubeStats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="subscriber_count", type="integer")
     */
    private $subscriberCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="view_count", type="integer")
     */
    private $viewCount;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment_count", type="integer")
     */
    private $commentCount;


    /**
     * @var integer
     *
     * @ORM\Column(name="video_count", type="integer")
     */
    private $videoCount;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subscriberCount
     *
     * @param integer $subscriberCount
     * @return YoutubeStats
     */
    public function setSubscriberCount($subscriberCount)
    {
        $this->subscriberCount = $subscriberCount;

        return $this;
    }

    /**
     * Get subscriberCount
     *
     * @return integer 
     */
    public function getSubscriberCount()
    {
        return $this->subscriberCount;
    }

    /**
     * Set viewCount
     *
     * @param integer $viewCount
     * @return YoutubeStats
     */
    public function setViewCount($viewCount)
    {
        $this->viewCount = $viewCount;

        return $this;
    }

    /**
     * Get viewCount
     *
     * @return integer 
     */
    public function getViewCount()
    {
        return $this->viewCount;
    }



    /**
     * Set commentCount
     *
     * @param integer $commentCount
     * @return YoutubeStats
     */
    public function setCommentCount($commentCount)
    {
        $this->commentCount = $commentCount;

        return $this;
    }

    /**
     * Get commentCount
     *
     * @return integer
     */
    public function getCommentCount()
    {
        return $this->commentCount;
    }



    /**
     * Set videoCount
     *
     * @param integer $videoCount
     * @return YoutubeStats
     */
    public function setVideoCount($videoCount)
    {
        $this->videoCount = $videoCount;

        return $this;
    }

    /**
     * Get videoCount
     *
     * @return integer
     */
    public function getVideoCount()
    {
        return $this->videoCount;
    }

    /**
     * Set date
     *
     * @param \Date $date
     * @return YoutubeStats
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \Date
     */
    public function getDate()
    {
        return $this->date;
    }
}
