<?php

namespace vox\AdminBundle\Entity;

use Doctrine\ORM\EntityRepository;


class VisitorRepository extends EntityRepository
{
    public function getActive() {

        $delay = new \DateTime();
        $delay->setTimestamp(strtotime('5 minutes ago'));

        $qb = $this->createQueryBuilder('u')
            ->where('u.lastActivity > :delay')
            ->setParameter('delay', $delay);

        return $qb->getQuery()->getResult();
    }

    public function getVisitorsToday() {
        $delay = new \DateTime();
        $delay->setTimestamp(strtotime('5 minutes ago'));

        $qb = $this->createQueryBuilder('v')
            ->where('v.lastActivity > CURRENT_DATE()')
            ->orderBy("v.lastActivity", 'DESC')
            ->getQuery()
            ->getResult();


        return $qb;
    }
}