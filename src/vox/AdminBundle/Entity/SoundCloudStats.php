<?php

namespace vox\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SoundCloudStats
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SoundCloudStats
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="followers", type="integer")
     */
    private $followers;

    /**
     * @var integer
     *
     * @ORM\Column(name="tracks", type="integer")
     */
    private $tracks;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set followers
     *
     * @param integer $followers
     * @return SoundCloudStats
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;

        return $this;
    }

    /**
     * Get followers
     *
     * @return integer 
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * Set tracks
     *
     * @param integer $tracks
     * @return SoundCloudStats
     */
    public function setTracks($tracks)
    {
        $this->tracks = $tracks;

        return $this;
    }

    /**
     * Get tracks
     *
     * @return integer
     */
    public function getTracks()
    {
        return $this->tracks;
    }

    /**
     * Set date
     *
     * @param \Date $date
     * @return SoundCloudStats
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \Date
     */
    public function getDate()
    {
        return $this->date;
    }
}
