<?php

namespace vox\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VisitorHistory
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class VisitorHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="visitor_count", type="integer")
     */
    private $visitorCount;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \Date $date
     *
     * @return VisitorHistory
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \Date
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set visitorCount
     *
     * @param integer $visitorCount
     *
     * @return VisitorHistory
     */
    public function setVisitorCount($visitorCount)
    {
        $this->visitorCount = $visitorCount;

        return $this;
    }

    /**
     * Get visitorCount
     *
     * @return integer
     */
    public function getVisitorCount()
    {
        return $this->visitorCount;
    }
}

