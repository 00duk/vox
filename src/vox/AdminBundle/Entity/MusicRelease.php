<?php

namespace vox\AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MusicReleaseEntity
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class MusicRelease
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="catalog", type="string", length=255)
     */
    private $catalog;

    /**
     * @var string
     *
     * @ORM\Column(name="format", type="string", length=255)
     */
    private $format;

    /**
     * @var string
     *
     * @ORM\Column(name="tracklist", type="text", nullable= true)
     */
    private $tracklist;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable= true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="shop_link", type="text", nullable= true)
     */
    private $shopLink;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_added", type="datetime", nullable= true)
     */
    private $dateAdded;

    /**
     * @var \Date
     *
     * @ORM\Column(name="release_date", type="date", nullable= true)
     */
    private $releaseDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    private $enabled;

    /**
     * @ORM\OneToOne(targetEntity="vox\AdminBundle\Entity\Image", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $image = null;



    public function __construct() {
        $this->dateAdded = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return ReleaseEntity
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Set catalog
     *
     * @param string $catalog
     * @return ReleaseEntity
     */
    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;

        return $this;
    }

    /**
     * Get catalog
     *
     * @return string
     */
    public function getCatalog()
    {
        return $this->catalog;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return ReleaseEntity
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set tracklist
     *
     * @param string $tracklist
     * @return ReleaseEntity
     */
    public function setTracklist($tracklist)
    {
        $this->tracklist = $tracklist;

        return $this;
    }

    /**
     * Get tracklist
     *
     * @return string 
     */
    public function getTracklist()
    {
        return $this->tracklist;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ReleaseEntity
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Set shopLink
     *
     * @param string $shopLink
     * @return ReleaseEntity
     */
    public function setShopLink($shopLink)
    {
        $this->shopLink = $shopLink;

        return $this;
    }

    /**
     * Get shopLink
     *
     * @return string
     */
    public function getShopLink()
    {
        return $this->shopLink;
    }

    /**
     * Set dateAdded
     *
     * @param \Date $dateAdded
     * @return ReleaseEntity
     */
    public function setDateAdded($dateAdded)
    {
        $this->dateAdded = $dateAdded;

        return $this;
    }

    /**
     * Get dateAdded
     *
     * @return \Date
     */
    public function getDateAdded()
    {
        return $this->dateAdded;
    }

    /**
     * Set releaseDate
     *
     * @param \DateTime $releaseDate
     * @return ReleaseEntity
     */
    public function setReleaseDate($releaseDate)
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * Get releaseDate
     *
     * @return \DateTime
     */
    public function getReleaseDate()
    {
        return $this->releaseDate;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     * @return ReleaseEntity
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean 
     */
    public function getEnabled()
    {
        return $this->enabled;
    }



    public function getImage()
    {
        return $this->image;
    }


    public function setImage(Image $image = null)
    {
        $this->image = $image;
    }



}
