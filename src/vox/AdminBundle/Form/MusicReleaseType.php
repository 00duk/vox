<?php

namespace vox\AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MusicReleaseType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            /*->add('artist_name',      'text', array('attr' => array('class' => 'form-control', 'autocomplete' => 'off')))*/
            ->add('title',     'text', array('attr' => array('class' => 'form-control', 'autocomplete' => 'off')))
            ->add('catalog',     'text', array('attr' => array('class' => 'form-control', 'autocomplete' => 'off')))
            ->add('format',     'text', array('attr' => array('class' => 'form-control', 'autocomplete' => 'off')))
            ->add('tracklist',    'textarea', array('attr' => array('class' => 'form-control'), 'required' => false))
            ->add('description',   'textarea', array('attr' => array('class' => 'form-control', 'rows' => 7), 'required' => false))
            ->add('shopLink',    'text', array('attr' => array('class' => 'form-control'), 'required' => false))
            ->add('release_date',   'date', array(
                'widget' => 'single_text',
                'format' => 'yyyy-MM-dd',
                'required' => false
            ), array('attr' => array('class' => 'form-control')))
            ->add('image', new ImageType(), array('required' => false))
            ->add('enabled', 'checkbox', array('required' => false, 'attr' => array('checked' => 'checked')))
            ->add('save',      'submit', array('attr' => array('class' => 'btn btn-default')))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'vox\AdminBundle\Entity\MusicRelease'
        ));
    }

    public function getName()
    {
        return 'vox_musicrelease_form';
    }

} 