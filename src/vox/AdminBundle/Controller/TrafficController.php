<?php

namespace vox\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use vox\AdminBundle\Entity\Visitor;

class TrafficController extends Controller
{
    /**
     * @Route("/admin/traffic", name="traffic_index")
     * @Template()
     */
    public function traffic_indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $visitorRep = $em->getRepository('voxAdminBundle:Visitor');
        $visitorsToday = $visitorRep->getVisitorsToday();

        $visitorHistoryRep = $em->getRepository('voxAdminBundle:VisitorHistory');

        $visitorHistory = $visitorHistoryRep->createQueryBuilder('v')->setMaxResults(30)->getQuery()->getResult();

        $last30days = array(array('Date', 'Visits'));
        foreach($visitorHistory as $day) {
            $date = $day->getDate();
            $date = $date->format('d M');
            $last30days[] = array($date, $day->getVisitorCount());
        }

        $countriesResult = $visitorRep->createQueryBuilder('v')->select('v.country, COUNT(v.country) as countryCount')->groupBy('v.country')->getQuery()->getResult();


        $countries = array(array('Country', 'Visits'));
        foreach($countriesResult as $country) {
            $countries[] = array($country['country'], (int) $country['countryCount']);
        }


        return array(
            'visitorsToday' => $visitorsToday,
            'last30days' => json_encode($last30days),
            'countries' => json_encode($countries)
        );
    }

    /**
     * @Route("/admin/visitor/{id}", name="visitor_details")
     * @Template()
     */
    public function visitor_detailsAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('voxAdminBundle:Visitor');

        $visitorDetails = $repository->find($id);
        return array('visitorDetails' => $visitorDetails);
    }
}
