<?php

namespace vox\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    /**
     * @Route("/admin/dashboard", name="dashboard_index")
     * @Route("/admin/", name="admin")
     * @Template()
     */
    public function dashboard_indexAction()
    {
        $date_today = new \DateTime('now');
        #$logs = file_get_contents("/var/log/vox/vox-" . $date_today->format('Y-m-d') . ".log");
        #$logs = file_get_contents("/var/log/vox/vox.log");

        $logFile = '/var/log/vox/vox.log';
        $command = "tac $logFile > /tmp/reversedlogfile.txt";
        exec($command);
        $currentRow = 0;
        $numRows = 50;
        $handle = fopen("/tmp/reversedlogfile.txt", "r");
        while (!feof($handle) && $currentRow <= $numRows) {
            $currentRow++;
            $buffer = fgets($handle, 4096);

            if(!strpos($buffer, 'Populated SecurityContext with an anonymous')) {
                $logs[] = str_replace(' [] []', '', $buffer);
            }

        }
        fclose($handle);
        $command = "rm /tmp/reversedlogfile.txt";
        exec($command);

        $em = $this->getDoctrine()->getManager();
        $visitorsToday = count($em->getRepository('voxAdminBundle:Visitor')->getVisitorsToday());
        $visitorsOnline = count($em->getRepository('voxAdminBundle:Visitor')->getActive());


        $fbRep = $em->getRepository('voxAdminBundle:FacebookStats');
        $fbStats = $fbRep->findOneByDate(new \DateTime('now'));
        $fb = array('likes' => 0, 'shares' => 0);
        if($fbStats) {
            $fb['likes'] = $fbStats->getLikes();
            $fb['shares'] = $fbStats->getShares();
        }


        $scRep = $em->getRepository('voxAdminBundle:SoundCloudStats');
        $scStats = $scRep->findOneByDate(new \DateTime('now'));
        $sc = array('followers' => 0, 'tracks' => 0);
        if($scStats) {
            $sc['followers'] = $scStats->getFollowers();
            $sc['tracks'] = $scStats->getTracks();
        }

        $ytRep = $em->getRepository('voxAdminBundle:YoutubeStats');
        $ytStats = $ytRep->findOneByDate(new \DateTime('now'));
        $yt = array('subscriberCount' => 0, 'viewCount' => 0, 'commentCount' => 0, 'videoCount' => 0);
        if($ytStats) {
            $yt['subscriberCount'] = $ytStats->getSubscriberCount();
            $yt['viewCount'] = $ytStats->getViewCount();
            $yt['commentCount'] = $ytStats->getCommentCount();
            $yt['videoCount'] = $ytStats->getVideoCount();
        }

        return array('logs' => $logs, 'visitorsToday' => $visitorsToday, 'visitorsOnline' => $visitorsOnline, 'fb' => $fb, 'sc' => $sc, 'yt' => $yt);
    }

}
