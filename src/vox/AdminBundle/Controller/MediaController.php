<?php

namespace vox\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class MediaController extends Controller
{
    /**
     * @Route("/admin/media", name="media_index")
     * @Template()
     */

    public function media_indexAction()
    {
        return array();
    }


    /**
     * @Route("/admin/get_chart_data", name="get_chart_data")
     */

    public function getChartData(Request $request)
    {
        $data_request = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $dt_from = date("Y-m-d", strtotime("2 months ago"));

        if ($data_request == "youtube") {

            $query = $em->createQuery(
                'SELECT y FROM voxAdminBundle:YoutubeStats y
                WHERE y.date > :date_from
                ORDER BY y.date ASC'
            )->setParameter('date_from', $dt_from);

            $youtubeStats = $query->getResult();

            $youtubeSubscriber = array(array('Date', 'Subscribers'));
            foreach ($youtubeStats as $stat) {
                $youtubeSubscriber[] = array(
                    $stat->getDate()->format('d M'),
                    $stat->getSubscriberCount()
                );
            }

            $youtubeView = array(array('Date', 'Views'));
            foreach ($youtubeStats as $stat) {
                $youtubeView[] = array(
                    $stat->getDate()->format('d M'),
                    $stat->getViewCount(),
                );
            }

            $youtubeComment = array(array('Date', 'Comments'));
            foreach ($youtubeStats as $stat) {
                $youtubeComment[] = array(
                    $stat->getDate()->format('d M'),
                    $stat->getCommentCount(),
                );
            }

            $youtubeVideo = array(array('Date', 'Uploads'));
            foreach ($youtubeStats as $stat) {
                $youtubeVideo[] = array(
                    $stat->getDate()->format('d M'),
                    $stat->getVideoCount(),
                );
            }

            $youtubeData['subscriber'] = $youtubeSubscriber;
            $youtubeData['view'] = $youtubeView;
            $youtubeData['comment'] = $youtubeComment;
            $youtubeData['video'] = $youtubeVideo;

            return new Response(json_encode($youtubeData));
        }

        if($data_request == "facebook") {

            $query = $em->createQuery(
                'SELECT f FROM voxAdminBundle:FacebookStats f
                WHERE f.date > :date_from
                ORDER BY f.date ASC'
            )->setParameter('date_from', $dt_from);

            $facebookStats = $query->getResult();

            $facebookLikes = array(array('Date', 'Likes'));
            foreach ($facebookStats as $stat) {
                $facebookLikes[] = array($stat->getDate()->format('d M'), $stat->getLikes());
            }

            $facebookShares = array(array('Date', 'Shares'));
            foreach ($facebookStats as $stat) {
                $facebookShares[] = array($stat->getDate()->format('d M'), $stat->getShares());
            }

            $facebookData['likes'] = $facebookLikes;
            $facebookData['shares'] = $facebookShares;

            return new Response(json_encode($facebookData));
        }

        if($data_request == "soundcloud") {

            $query = $em->createQuery(
                'SELECT s FROM voxAdminBundle:SoundCloudStats s
                WHERE s.date > :date_from
                ORDER BY s.date ASC'
            )->setParameter('date_from', $dt_from);
            $soundcloudStats = $query->getResult();

            $soundcloudFollowers = array(array('Date', 'Followers'));
            foreach ($soundcloudStats as $stat) {
                $soundcloudFollowers[] = array($stat->getDate()->format('d M'), $stat->getFollowers());
            }

            $soundcloudTracks = array(array('Date', 'Tracks'));
            foreach ($soundcloudStats as $stat) {
                $soundcloudTracks[] = array($stat->getDate()->format('d M'), $stat->getTracks());
            }

            $soundcloudData['followers'] = $soundcloudFollowers;
            $soundcloudData['tracks'] = $soundcloudTracks;

            return new Response(json_encode($soundcloudData));
        }

        return new Response();
    }


}
