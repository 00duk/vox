<?php

namespace vox\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Google_Client;
use Google_Service_Analytics;
use Google_Auth_AssertionCredentials;



class AnalyticsController extends Controller
{

    /**
     * @Route("/admin/analytics", name="analytics_index")
     * @Template("")
     */
    public function analytics_indexAction()
    {


        return array();
    }


    /**
     * @Route("/admin/analytics_update_overview", name="analytics_update_overview")
     * @Template("")
     */

    public function update_overviewAction(Request $request) {

        $range = json_decode($request->getContent());
        if(!empty($range->from) && !empty($range->to)) {
            $date_from = $range->from;
            $date_to = $range->to;
        } else {
            $date_from = '28daysAgo';
            $date_to = 'today';
        }

        $client = new Google_Client();
        $fileLocator = $this->get('file_locator');
        $key_path = $fileLocator->locate('@voxAdminBundle/Resources/ga/37fc1ced5398.p12');
        $key = file_get_contents($key_path);

        $cred = new Google_Auth_AssertionCredentials(
            '886960317184-c348ccsvpa8n4aprkbge0pp5k65ic068@developer.gserviceaccount.com',
            array('https://www.googleapis.com/auth/analytics.readonly'),
            $key
        );

        $client->setAssertionCredentials($cred);


        if($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }



        $analytics = new Google_Service_Analytics($client);
        $analytics_id   = 'ga:94491599';
        #$lastWeek       = date('Y-m-d', strtotime('-52 week'));
        #$start_date       = '7daysAgo';
        $start_date       = $date_from;
        $end_date          = $date_to;
        $optParams = array();
        $metrics = 'ga:users, ga:sessions, ga:avgSessionDuration, ga:pageviews, ga:pageviewsPerSession';
        // Uncomment any of the optional parameters to include them in your query
        $optParams['dimensions'] = "ga:date";
        //$optParams['sort'] = "";
        //$optParams['filters'] = "";
        //$optParams['max-results'] = "";

        try {
            $overview = $analytics->data_ga->get(
                $analytics_id,
                $start_date,
                $end_date,
                $metrics,
                $optParams);

        } catch(Exception $e) {
            echo 'There was an error : - ' . $e->getMessage();
        }


        $content = array();

        #var_dump($overview['rows']);

        foreach($overview['rows'] as $row) {

            $array['date'] = date('Y-m-d', strtotime($row[0]));
            $array['users'] = $row[1];
            $array['sessions'] = $row[2];
            $array['avgSessionDuration'] = round($row[3], 2);
            $array['pageviews'] = $row[4];
            $array['pageviewsPerSession'] = round($row[5], 2);

            array_push($content, $array);

            unset($array);
           /* foreach($row as $elem) {
            }*/
        }
        #$data = array("data"=> $content);
        /*var_dump($content);
        die;*/

        $json_overview = json_encode($content);

        return new Response($json_overview);
    }
}
