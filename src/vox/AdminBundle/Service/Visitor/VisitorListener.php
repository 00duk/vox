<?php

namespace vox\AdminBundle\Service\Visitor;

use vox\AdminBundle\Entity\Visitor;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Doctrine\ORM\EntityManager;

class VisitorListener {

    #protected $visitorInfo;
    protected $em;

    public function __construct(EntityManager $em)
    {
        #$this->visitorInfo = $visitorInfo;
        $this->em = $em;
    }

    public function processVisitorInfo(FilterResponseEvent $event)
    {
        if (!$event->isMasterRequest())
            return;


        $uri = $_SERVER['REQUEST_URI'];
        $ip = $_SERVER['REMOTE_ADDR'];
        $query_string = $_SERVER['QUERY_STRING'];
        @$http_referer = $_SERVER['HTTP_REFERER'];
        @$http_user_agent = $_SERVER['HTTP_USER_AGENT'];

        $repository = $this->em->getRepository('voxAdminBundle:Visitor');
        $visitor = $repository->findOneByIp($ip);

        if($visitor) {
            $visitor->setLastActivity(new \DateTime('now'));
            $this->em->persist($visitor);
            $this->em->flush();
            return;
        }
        else {
            $visitor = new  Visitor();
            $visitor->setIp($ip);
            $visitor->setHttpReferer($http_referer);
            $visitor->setHttpUserAgent($http_user_agent);
            $visitor->setQueryString($query_string);
            $visitor->setLastActivity(new \DateTime('now'));

            $this->em->persist($visitor);
            $this->em->flush();
            return;
        }




    }
}