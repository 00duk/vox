<?php

namespace vox\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use vox\AdminBundle\Entity\MusicRelease;
use Symfony\Component\HttpFoundation\Request;

class StaticPagesController extends Controller
{
    /**
     * @Route("/about", name="front_about")
     * @Template("")
     */
    public function aboutAction()
    {


        return array();
    }
}