<?php

namespace vox\FrontBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Validator\Constraints\Email;
use vox\AdminBundle\Entity\MusicRelease;
use Symfony\Component\HttpFoundation\Request;
use vox\FrontBundle\Entity\Contact;
use vox\FrontBundle\Form\ContactType;

class ContactController extends Controller
{

    /**
     * @Route("/contact", name="front_contact")
     * @Template("")
     */

    public function contactAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->get('form.factory')->create(new ContactType(), $contact);

        $logger = $this->get('monolog.logger.vox');

        if ($form->handleRequest($request)->isValid()) {

            $errors = $this->validateEmails($contact->getEmail());

            if(count($errors) > 0) {
                $logger->warning('Visitor entered wrong email in contact form.');
                $request->getSession()->getFlashBag()->add('warning', 'Invalid email address.');
                return $this->redirect($this->generateUrl('front_contact'));
            }

            $em = $this->getDoctrine()->getManager();
            $contact->setIp($this->get('request')->getClientIp());
            $contact->setTimeSent(new \DateTime('now'));
            $em->persist($contact);
            $em->flush();


            $logger->info('New email from ' . $contact->getEmail() . '. Category: ' . $contact->getCategory() . ', Subject: ' . $contact->getSubject());

            $categoryAddr = array(
                'general' => 'contact@voxpopuli-records.com',
                'booking' => 'booking@voxpopuli-records.com',
                'demo' => 'demo@voxpopuli-records.com'
            );

            $send_to = $categoryAddr[$contact->getCategory()];

            $this->sendMail($contact, $send_to);

            $request->getSession()->getFlashBag()->add('notice', 'Your message was successfully sent!');
            return $this->redirect($this->generateUrl('front_contact'));
        }

        return array('form' => $form->createView());
    }


    private function sendMail($contact, $send_to) {

        $message = \Swift_Message::newInstance()
            ->setSubject($contact->getSubject())
            ->setFrom($contact->getEmail())
            ->setTo($send_to)
            ->setBody($contact->getMessage(), 'text/html')
        ;
        $this->get('mailer')->send($message);
    }

    public function validateEmails($emails)
    {
        $errors = array();
        $emails = is_array($emails) ? $emails : array($emails);

        $validator = $this->get('validator');

        $constraints = array(
            new \Symfony\Component\Validator\Constraints\Email(),
            new \Symfony\Component\Validator\Constraints\NotBlank()
        );

        foreach ($emails as $email) {

            $error = $validator->validateValue($email, $constraints);

            if (count($error) > 0) {
                $errors[] = $error;
            }
        }

        return $errors;

    }
}