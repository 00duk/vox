<?php

namespace vox\FrontBundle\Twig;

class UrlExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('convert_url', array($this, 'convertUrl')),
        );
    }

    public function convertUrl($string)
    {
        $string = preg_replace("/([^\w\/])(www\.[a-z0-9\-]+\.[a-z0-9\-]+)/i", "$1http://$2",$string);
        /*** make all URLs links ***/
        $string = preg_replace("/([\w]+:\/\/[\w-?&;#~=\.\/\@]+[\w\/])/i","<a target=\"_blank\" href=\"$1\">$1</a>",$string);

        return $string;
    }

    public function getName()
    {
        return 'url_extension';
    }
}