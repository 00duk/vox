<?php

namespace vox\FrontBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', 'choice', array(
                'choices' => array(
                    'general'   => 'General',
                    'booking' => 'Booking',
                    'demo'   => 'Demo',
                ), 'attr' => array('class' => 'form-control')))
            ->add('email', 'text', array('attr' => array('class' => 'form-control', 'autocomplete' => 'off', "maxlength" => '80')))
            ->add('subject', 'text', array('attr' => array('class' => 'form-control', 'autocomplete' => 'off')))
            ->add('message', 'textarea', array('attr' => array('class' => 'form-control', 'rows' => 5)))
            ->add('send', 'submit', array('attr' => array('class' => 'btn btn-default')))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'vox\FrontBundle\Entity\Contact'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'vox_frontbundle_contact';
    }
}
