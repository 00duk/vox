set :application, "vox"
set :domain,      "digitalocean"
set :user,        "root"
set :deploy_to,   "/var/www/vox-deploy"
set :app_path,    "app"





set :repository,  "git@bitbucket.org:00duk/vox.git"
set :scm,         :git
set :branch, "master"
set :deploy_via, :remote_cache

# shared files
set :shared_files, ["app/config/parameters.yml"]

# shared children
set :shared_children, [app_path + "/logs", "vendor"]
set :writable_dirs, [app_path + "/cache", app_path + "/logs", web_path + "/uploads"]
set :webserver_user,    "www-data"
set :permission_method, :acl
set :use_set_permissions, true

# Or: `accurev`, `bzr`, `cvs`, `darcs`, `subversion`, `mercurial`, `perforce`, or `none`
# Or: `propel`

role :web,        domain                         # Your HTTP server, Apache/etc
role :app,        domain, :primary => true       # This may be the same as your `Web` server

set  :keep_releases,  3

# Be more verbose by uncommenting the following line
#logger.level = Logger::MAX_LEVEL



# Composer settings
set :use_composer, true
#set :update_vendors, true
set :vendors_mode, "install"

set :model_manager, "doctrine"